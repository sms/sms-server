# sms-server

This can set up 3 servers:

## input

https://[host]/sms/inputfeeds.html lists all inputfeeds with a player, qr code, and their status.

send video to: rtmps://[host]:1936/input/name or rtmp://[host]:1935/input/name

## trancoder

send video to: rtmp://[host]:1937/streams/name

Then different variants of hls will be generated on: https://[host]/hls/transcoded/
And the original on: https://[host]/hls/streams

Player on https://[host]/sms/player.html

## studio

TODO



# To develop / test



docker-compose up --build test

Test the input by running test/testinputloop.sh
