  var hlsConfig = {

    }

var playertype = 'ovenplayer';



function makeplayer(elem, url) {

	switch(playertype) {
		case 'hls.js':

			hls = new Hls(hlsConfig); 

			hls.loadSource(url);
			
			hls.attachMedia(document.getElementById("player"));
			
			hls.on(Hls.Events.MANIFEST_PARSED,function() {
					feed.player.play();
			});
			return hls;
			break;
		case 'ovenplayer':
			const player = OvenPlayer.create(elem, {
				"autoStart": true,
				"autoFallback": true,
				"mute": false,
				"sources": [
 //       {
//					"type": "hls",
//					"file": url
//				},
        {
          "type": "dash",
          "file": "https://hls.laglab.org/hls/dash_streams/test.mpd"
        }
        ]
			});
			return player;	

			break;
		default:
			// code block
	}


}

function init() {

  if ( location.protocol == 'file:') {
    var hlsserver = 'https://hls.laglab.org';
  } else {
    if (location.pathname == '/sms/player.html') {
      var hlsserver = location.protocol + "//" + location.hostname + ":" + location.port;
      var streamname = window.location.hash.slice(1);
    } else {
      var hlsserver = 'https://hls.laglab.org';
      var streamname = 'test'
    }
  }

  makeplayer(document.getElementById("sms_player"), hlsserver + "/hls/transcoded/" + streamname + ".m3u8");

}

/* you give it a div, and it will return a player (so its possible to add some extra controls */
function sms_include_player(div, streamname) {
  console.log('making player: ', div, streamname)
  
  const video = document.createElement('video');
  video.width = 1280;
  video.height = 720;
  video.controls = true;
  video.autoplay = true;
  
  div.appendChild(video);
	var hlsserver = 'https://hls.laglab.org'
  makeplayer(video, hlsserver + "/hls/transcoded/" + streamname + ".m3u8");
 
  return video
}

