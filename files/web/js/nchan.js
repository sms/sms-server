function initNchan () {
// CommonJS
//let NchanSubscriber = require("nchan");

opt = {
  subscriber: 'websocket',
  //reconnect: undefined
  //shared: undefined;
};

var sub = new NchanSubscriber(thisserver + "/sub", opt);
sub.on("transportSetup", function(opt, subscriberName) {
  console.log("transportSetup", opt, subscriberName);
  // opt is a hash/object - not all transports support all options equally. Only longpoll supports arbitrary headers
  // subscriberName is a string
  //
  // longpoll transport supports;
  //   opt.longpoll.pollDelay - delay in milliseconds between successful requests
});

sub.on("transportNativeCreated", function(nativeTransportObject, subscriberName) {
  console.log("transportNativeCreated", subscriberName);

  // nativeTransportObject is the native transport object and depends on the subscriber type
  // subscriberName is a string
});

sub.on("transportNativeBeforeDestroy", function(nativeTransportObject, subscriberName) {
  console.log("transportNativeBeforeDestroy", subscriberName);


  // nativeTransportObject is the native transport object and depends on the subscriber type
  // subscriberName is a string
});

sub.on("message", function(message, message_metadata) {
  console.log("message: ", message, message_metadata);
  
  var pre = document.createElement("p"); 
  var textNode = document.createTextNode("message " + message_metadata.id + ": " + message);
  pre.appendChild(textNode);
  document.getElementById("nchan").appendChild(pre);


  // message is a string
  // message_metadata is a hash that may contain 'id' and 'content-type'
});

sub.on('connect', function(evt) {
  console.log('connect', evt);
});

sub.on('disconnect', function(evt) {
  console.log('disconnect', evt);
});






//sub.on('error', function(error_code or evt, error_description) {
//  console.log("error: ", evt, error_description);
//  //error callback
//});
 
//sub.reconnect; // should subscriber try to reconnect? true by default.
//sub.reconnectTimeout; //how long to wait to reconnect? does not apply to EventSource, which reconnects on its own.
//sub.lastMessageId; //last message id. useful for resuming a connection without loss or repetition.



  sub.start(); // begin (or resume) subscribing
}
//sub.stop(); // stop subscriber. do not reconnect.
