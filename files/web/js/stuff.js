
let thisserver = location.protocol + "//" + location.hostname + ":" + location.port


function generateUrl(name, protocol) {
  var resolution			 = encodeURIComponent("1280x720");
  var bitrate					 = "2000"; 
  var server = location.hostname;

  if ( protocol == 'rtmp') { 
    // var rtmpUr      = "rtmp://hlsdev.laglab.org/input/";
    var rtmpUrl     = "rtmp://" + location.hostname + "/" + defaultApplication + "/" + name
    var groveUrl					 = 'larix://set/v1?conn[][url]=' + encodeURIComponent(rtmpUrl) + '&conn[][name]=' + encodeURIComponent(name) + '&conn[][mode]=av&conn[][overwrite]=on&enc[vid][camera]=0&enc[vid][res]=' + resolution + '&enc[vid][fps]=30&enc[vid][bitrate]=' + bitrate + '&enc[aud][bitrate]=128&enc[aud][samples]=44100&enc[aud][channels]=2'

   return groveUrl;
  }

  if (protocol == 'srt') {

  
    //srt://noise.puscii.nl:1337/publish/srt_test
  var srtUrl = 'larix://set/v1?conn[][url]=srt%3A%2F%2F' + server + '%3A1337&conn[][srtstreamid]=publish%2Fsrt_' + name + '&tb[][url]=srt%3A%2F%2F' + server + '%3A1337&tb[][name]=studio%20talkback&tb[][srtstreamid]=play%2Ftalkback&tb[][overwrite]=on&enc[vid][camera]=0&enc[vid][res]=1280x720&enc[vid][fps]=25&enc[vid][format]=avc&enc[vid][bitrate]=2500&enc[vid][adaptiveBitrate]=3&enc[aud][samples]=48000'
    return srtUrl
  }
}

function makeLarixUrl ( feed ) {

  return generateUrl(feed.name, "srt");
}



//var defaultServer = "localhost"
var defaultApplication = "input"
 function hlsTryLoad() {
       //console.log("retrying load: ",this.currentLevel, this, this.url)

        if (this.currentLevel == -1) {

       // console.log("retrying load: ",this.currentLevel, this, this.url)
          this.loadSource(this.url);
          this.startLoad();
        } else {
          console.log("not retrying load, currentlevel: ",this.currentLevel, this.url)
        }

    }


    function hlsError(event, data) {
      if (typeof data != 'undefined' && typeof data.type != 'undefined') {

        //console.log("hlserror: ", event, data, this, data.type)
        switch (data.type) {
            case Hls.ErrorTypes.NETWORK_ERROR:    // playlist file 404'd (probably), wait 1 second and retry
            //    console.log("network error on playlist load, not doing anything", this, event, data.type, data.details, data.fatal, data);
                if (data.fatal == true) {
             //     console.log("fatal network error on playlist load", this, event, data.type, data.details, data.fatal, data);
                  if (data.details == 'manifestLoadError') {
                    //console.log("manifest load error, retrying in 500 ms.", this, event, data.type, data.details, data.fatal, data);

                    const boundHlsTry = hlsTryLoad.bind(this.hls);
                    this.hlsTryLoadTimer = setTimeout(() => boundHlsTry(), 500);
                  }
                }
        }
      }
    }
 
    function hlsEvent(event, data) {
      console.log("hlsevent: ", event, data, this, data.type)
    }
 

    function hlsReady(event, data) {
        console.log("hlsready: ", event, data, this, data.type)
       
     // if (typeof data != 'undefined' && typeof data.type != 'undefined') {

       this.player.play();
     // }
    }

    var hlsConfig = {
      manifestLoadingTimeOut: 100,
      manifestLoadingMaxRetry: 5,
      manifestLoadingRetryDelay: 100,
      manifestLoadingMaxRetryTimout: 100,
      maxBufferLength: 60,
      liveSyncDurationCount: 4,

    }
 
//const thisurl = new URL(location);

//let hostname = location.hostname;
//let port = location.port;
//let protocol = location.protocol;

let hlsserver = location.protocol + "//" + location.hostname + ":" + location.port

//console.log("hlsserver: ", hlsserver)

/*
const getCircularReplacer = () => {
  const seen = new WeakSet();
  return (key, value) => {
    if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};
*/

function jsonReplacer(key, value) {
//  console.log("asdfasdfs: ", key, value);
  if (key === 'hls') {
 //   console.log("iskey: ", key, value)
    return undefined;
  }
  return value;
}

function getHlsUrl( feed ) {
  return hlsserver + "/hls/inputhls/" + feed.name + ".m3u8"
}


var feedDefault = {
  //  width: 1280,
  //  height: 720,
  //  fps: 30,
    la: "defaults"
}  

var configuredFeeds = {
  "preconfigured1": {
    la: "feed1",
  },
  "preconfigured2": {
    la: "feed2"
  },
 "preconfigured3": {
    la: "feed3"
  }

}; 

var feeds = {};

function refreshFeed(feed) {
      const now = Date.now()
      
      if (typeof feed.live == 'number') {
        const lastlive = now - feed.live;
        //console.log("livetest: ", lastlive, feed.live, typeof feed.live)

        if (lastlive < 2000 ) {
          feed.ul.style.background = 'lightgreen';

          if ( feed.state == "dead" ) {
            feed.state = "alive";
            console.log("hls when going green: ", feed.hls, feed.player);

            
            // destroy the hls thing, so it does not get confused
            feed.hls.destroy();
        
            feed.hls = createHls(feed);

          }

        } else if (lastlive > 12000) {
          feed.ul.style.background = 'red';
           // destroy the hls thing, so it does not get confused
           feed.hls.destroy();
        

          feed.state = "dead";
        } else {
          feed.ul.style.background = 'yellow';
            // destroy the hls thing, so it does not get confused
           feed.hls.destroy();
        

           feed.state = "dead";

        }
      }



}

function createHls(feed) {
    hls = new Hls(hlsConfig); 
    let r = (Math.random() + 1).toString(36).substring(7);

    const boundHlsTry = hlsTryLoad.bind(hls);         
    const boundHlsError = hlsError.bind(feed);
    const boundHlsReady = hlsReady.bind(feed);
    const boundHlsEvent = hlsEvent.bind(feed);



    hls.on(Hls.Events.ERROR, boundHlsError);
    hls.on(Hls.Events.MANIFEST_PARSED, boundHlsReady);
    hls.on(Hls.Events.BUFFER_EOS, boundHlsEvent);
    hls.on(Hls.Events.MEDIA_DETACHED, boundHlsEvent);
    hls.on(Hls.Events.DESTROYING, boundHlsEvent);
//    feed.hls.on(Hls.Events.ERROR, boundHlsEvent);

    hls.loadSource(feed.url + "?randomforcache=" + r);
    
    hls.attachMedia(feed.player);
    
    hls.on(Hls.Events.MANIFEST_PARSED,function() {
        feed.player.play();
    });

  return hls;
}



function feedChanged(changes) {
  for (const [k, v] of Object.entries(changes)) {
    //console.log("changed: ", k, v)
    v.object.ul.querySelector("pre").firstChild.nodeValue = JSON.stringify(v.object,  jsonReplacer);
    //console.log( "mmm: ", v.object , JSON.stringify(v.object,  jsonReplacer));
    switch (v.name) {
      case "live":
        //console.log("live changed: ", v.object, v.object.name);
        refreshFeed(v.object);
      break;
    }
  }
}

function makeQrCode (canvas, str) {
  QRCode.toCanvas(canvas, str, {width: 500, margin: 0});
  canvas.style.width = "100px";
  canvas.style.height = "100px";
}

function initFeed(feed, name) {

  if (feed.initialized == true) {
    console.log("You can only initialize once");
  } else {
    // set name
    feed.name = name;

    //console.log("init feed: ", feed, name);

    // create UL
    feed.ul = document.createElement("div");
    feed.ul.classList.add('#feed');
    document.getElementById("allfeeds").appendChild(feed.ul);


    //title
    var h3 = document.createElement("h3"); 
    var textNode = document.createTextNode("streamname: " + feed.name);
    h3.appendChild(textNode);
    feed.ul.appendChild(h3);

    //json all info
    var pre = document.createElement("pre");
    var preText = document.createTextNode("whut: " + JSON.stringify(feed),  jsonReplacer);
    pre.appendChild(preText);
    feed.ul.appendChild(pre);

    //qrcode
    //
    var url = makeLarixUrl(feed)
    //console.log("larix: ", feed.name, url)

    var larixLink = document.createElement("a");
    larixLink.href = url;
    var larixLinkText = document.createTextNode("larix settings link ");
    larixLink.appendChild(larixLinkText);
    feed.ul.appendChild(larixLink);

    var qrcode = document.createElement("canvas");
    makeQrCode(qrcode, url);
    feed.ul.appendChild(qrcode);

    

    //player
    feed.player = document.createElement("video");
    feed.player.style.width = "160px";
    feed.player.style.height = "90px";
    feed.player.controls = true;
    feed.player.muted = true;
    feed.ul.appendChild(feed.player);

    feed.url = getHlsUrl(feed);

    feed.hls = createHls(feed);




    // create info
    

    //create player

    Object.observe(feed, feedChanged);
    feed.initialized = true
  }
  return feed
}

function getFeed( name ) {

  //console.log("getfeed for: ", name, "result: ", feeds[name])
  if ( feeds[name] == undefined ) {
    //console.log("undefined: ", name)
    feeds[name] = {}
    feeds[name] = Object.assign(feeds[name], feedDefault);
    initFeed(feeds[name], name)
  }
  //return Object.assign(feedDefault, feeds[name]);
  return feeds[name]
}


function init () {

  for (f in configuredFeeds) {
    feeds[f] = {}
    Object.observe(feeds[f], feedChanged);
    feeds[f] = Object.assign(feeds[f], feedDefault, configuredFeeds[f])
    initFeed(feeds[f], f)
  }
   
  window.setInterval(test, 1000);
}

function parseStats ( xmldoc ) {
  const applications = xml.getElementsByTagName("application");
  //console.log(applications)
  for (var i = 0; i < applications.length; i++) {
    //var name = names[i].firstChild.nodeValue;
    var name = applications[i].getElementsByTagName("name");
    var live = applications[i].getElementsByTagName("live");

    //console.log(name)
    //console.log(live)

    const streams = applications[i].getElementsByTagName("stream");


    if ( name[0].innerHTML == "input" ) {
      //console.log('match')
      for (var si = 0; si < streams.length; si++) {

        //console.log(streams[si])

        // var streamname = streams[si].getElementsByTagName("name").innerHTML;
        var streamname = streams[si].getElementsByTagName("name")[0].innerHTML
        var feed = getFeed(streamname);
        feed.live = Date.now();
        //console.log("got: ", feed);

      
      }

    }
  }
}


function renderStats ( xmldoc ) {
  const applications = xml.getElementsByTagName("application");
  //console.log(applications)
  for (var i = 0; i < applications.length; i++) {
    //var name = names[i].firstChild.nodeValue;
    var name = applications[i].getElementsByTagName("name");
    var live = applications[i].getElementsByTagName("live");

    //console.log(name)
    //console.log(live)

    const streams = applications[i].getElementsByTagName("stream");


    if ( name[0].innerHTML == "input" ) {
      //console.log('match')
      for (var si = 0; si < streams.length; si++) {

        //console.log(streams[si])

        // var streamname = streams[si].getElementsByTagName("name").innerHTML;
        var streamname = streams[si].getElementsByTagName("name")[0].innerHTML


        var div = document.createElement("div");
        var textNode = document.createTextNode("streamname: " + streamname);
        div.appendChild(textNode);
        document.getElementById("wrapper").appendChild(div);


      }



      var div = document.createElement("div");
      var textNode = document.createTextNode("name: " + name[0].innerHTML);
      div.appendChild(textNode);
      document.getElementById("wrapper").appendChild(div);

      var div = document.createElement("pre");
      var textNode = document.createTextNode("live: " + live[0].innerHTML);
      div.appendChild(textNode);
      document.getElementById("wrapper").appendChild(div);

    }
  }
}

function statsReqListener () {
  //console.log(this.responseText);
  //console.log(this.responseXML);
  xml = this.responseXML;
  parseStats(xml);
  //renderStats(xml);

}


function test() {

  var oReq = new XMLHttpRequest();
  oReq.addEventListener("load", statsReqListener);
  oReq.open("GET", hlsserver + "/stat");
  oReq.send();

  for (f in feeds) {
    refreshFeed(getFeed(f));
    //console.log("refreshing: ", f)
  }

}
