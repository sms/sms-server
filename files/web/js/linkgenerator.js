function makeQrCode (canvas, str) {
  QRCode.toCanvas(canvas, str, {width: 500, margin: 0});
  canvas.style.width = "600px";
  canvas.style.height = "600px";
}



function generateRandomUrl() {
	//	var name					   = "mobile reporter";
	//	var rtmpUrl				 	 = "rtmp://" + location.hostname + "/input";
	//	var resolution			 = "1280x720";
//		var bitrate					 = "2000"; 

  var streamKey        = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 8; i++ ) {
      streamKey += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
//	 var groveUrl					 = 'larix://set/v1?conn[][url]=' + encodeURIComponent(rtmpUrl) + streamKey + '&conn[][name]=' + encodeURIComponent(name) + '&conn[][mode]=av&conn[][overwrite]=on&enc[vid][camera]=0&enc[vid][res]=' + resolution + '&enc[vid][fps]=30&enc[vid][bitrate]=' + bitrate + '&enc[aud][bitrate]=128&enc[aud][samples]=44100&enc[aud][channels]=2'

    rtmpUrl = generateUrl(streamKey, "rtmp") 
    var larixLink = document.createElement("a");
    larixLink.href = rtmpUrl;
    var larixLinkText = document.createTextNode("larix rtmp settings link ");
    larixLink.appendChild(larixLinkText);
    document.getElementById("qrcode").appendChild(larixLink);

    var qrcode = document.createElement("canvas");
    makeQrCode(qrcode, rtmpUrl);
    document.getElementById("qrcode").appendChild(qrcode);

    srtUrl = generateUrl(streamKey, "srt") 
    var larixLink = document.createElement("a");
    larixLink.href = srtUrl;
    var larixLinkText = document.createTextNode("larix srt settings link ");
    larixLink.appendChild(larixLinkText);
    document.getElementById("qrcode").appendChild(larixLink);

    var qrcode = document.createElement("canvas");
    makeQrCode(qrcode, srtUrl);
    document.getElementById("qrcode").appendChild(qrcode);
}

