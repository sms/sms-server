# nat-wsmsg from: https://github.com/octu0/nats-wsmsg/releases/tag/v1.3.1
class smsserver::wsmsg(
)
{

  file { 
   '/usr/local/bin/nats-wsmsg':
    ensure => 'file',
    source => "puppet:///modules/${module_name}/nats-wsmsg",
    owner => 'root',
    group => 'root',
    mode  => '0755',
  }


  file { '/etc/systemd/system/nats-wsmsg.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/nats-wsmsg.service",
  }
  -> exec { '/usr/bin/systemctl daemon-reload':
  }
  -> exec { '/usr/bin/systemctl enable nats-wsmsg':
  }
 
  -> service {'nats-wsmsg':
    ensure => 'running',
  }

}


