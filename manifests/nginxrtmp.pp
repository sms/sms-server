# nginxrtmp
class smsserver::nginxrtmp (
  String  $hostname = 'localhost',
  String  $servertype = 'input',
)
{

  # https://github.com/Nesseref/nginx-rtmp-auth
  # https://github.com/Nesseref/html5-livestreaming

  
  $pkgs = [
    'libnginx-mod-nchan',
    'libnginx-mod-rtmp',
    'libnginx-mod-http-fancyindex',
    'nginx',
    'ffmpeg'
  ]


  ensure_packages($pkgs, {ensure => 'installed'})

  #disable default nginx page 
  file { '/etc/nginx/sites-enabled/default':
    ensure => absent
  }
  -> file { "/var/log/ffmpeg":
    ensure => directory,
    owner => 'www-data',
    mode => "0777",
  }

  #FIXME: do this properly, puppet does not support -p for mkdir
  -> file { '/mnt/radio':
    ensure => directory,
  }
  #FIXME: do this properly
  #-> file { '/var/www/hls':
  #  ensure => directory,
  #  owner => 'www-data',
  #}


  -> file { lookup(smsserver::record_path):
    ensure => directory,
  }


  -> file {  "${lookup(smsserver::record_path)}/input":
    ensure => directory,
    owner => 'www-data',
  }


  -> file { "${lookup(smsserver::record_path)}/output":
    ensure => directory,
    owner => 'www-data'
  }


  -> file { lookup(smsserver::hls_path):
    ensure => directory,
    owner  => 'www-data',
  }

  file { 
   '/var/www/sms':
    ensure => 'directory',
    source => "puppet:///modules/${module_name}/web",
    recurse => 'remote',
    path => '/var/www/sms',
    owner => 'www-data',
    group => 'www-data',
    mode  => '0744',
  }

  $nginxrtmp_conf = {
    hostname    => $hostname,
  }

  file { '/etc/nginx/sites-enabled/rtmp-http.conf':
    content => epp("${module_name}/nginx-rtmp-hls-http.conf.epp", $nginxrtmp_conf),
    notify  => Service['nginx'],
  }

  file { "/etc/nginx/modules-enabled/rtmp-input.conf":
    ensure => absent
  }

  file { "/etc/nginx/modules-enabled/rtmp-transcoder.conf":
    ensure => absent
  }


  file { "/etc/nginx/modules-enabled/rtmp.conf":
    content => epp("${module_name}/nginx-rtmp.conf.epp"),
    notify => Service['nginx'],
  }

  



}
