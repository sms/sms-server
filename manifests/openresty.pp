# openresty
class smsserver::openresty {

  require smsserver::openrestypkg

  $nginx_conf = {
    hostname    => $hostname,
  }

  service { 'openresty':
   # ensure  => 'running',
    enable  => true,
  }


 file { "/etc/resty-auto-ssl":
    ensure => directory,
    owner => 'www-data'
  }

  -> file { "/usr/local/openresty/nginx/conf/nginx.conf":
    content => epp("${module_name}/resty.conf.epp", $nginx_conf),
    notify => Service['openresty'],
  }
 

}
