# srt
class smsserver::srt (
  String  $hostname = 'localhost',
  String  $servertype = 'input',
)
{

  # https://github.com/Nesseref/nginx-rtmp-auth
  # https://github.com/Nesseref/html5-livestreaming

  
  $pkgs = [
    'libsrt-gnutls-dev',
    #libsrt-openssl-dev - Secure Reliable Transport UDP streaming library
    #libsrt1.4-gnutls - Secure Reliable Transport UDP streaming library (GnuTLS flavour)
    'libsrt1.4-openssl',
    'tcl',
    'cmake',
    'libssl-dev',
    'libz-dev',

  ]

  ensure_packages($pkgs, {ensure => 'installed'})
  
  put::buildfromgit { 'srt':
    revision => 'v1.4.4',
    repo      => 'https://github.com/Haivision/srt.git',
    commands  => [
      './configure',
      'make',
      'make install',
      'ldconfig'
    ]
    } 
  -> put::buildfromgit { 'srt-live-server':
    revision => 'master',
    repo => 'https://github.com/Edward-Wu/srt-live-server.git',
    commands  => [
      'make',
      'cp bin/sls /usr/local/bin | /bin/true',
      'chmod +x /usr/local/bin/sls | /bin/true',
    ],
  } 

  -> file { '/etc/systemd/system/sls.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/sls.service",
  }
  -> exec { '/usr/bin/systemctl daemon-reload for sls':
    command => '/usr/bin/systemctl daemon-reload'
  }
  -> service {'sls':
    ensure => 'running',
  }
  file { "/etc/sls.conf":
    content => epp("${module_name}/sls.conf.epp"),
    notify => Service['sls'],
  }




}
