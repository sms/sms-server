# auth
class smsserver::auth (
)
{

  
  $pkgs = [
    'golang',
    'protobuf-compiler',
    'protoc-gen-go',
    'golang-statik'
  ]

  ensure_packages($pkgs, {ensure => 'installed'})
  
  put::buildfromgit { 'rtmp-auth':
    revision => 'master',
    repo      => 'https://github.com/voc/rtmp-auth.git',
    commands  => [
      'go get github.com/rakyll/statik',
      'make',
    ]
    } 

    #./rtmp-auth -apiAddr "localhost:8000" -frontendAddr "localhost:8082" -config /usr/local/src/rtmp-auth/config.toml
  -> file { '/srv/rtmp-auth':
      ensure => directory,
      mode   => '0777',
  }
  -> file { '/etc/systemd/system/rtmp-auth.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    content => epp("${module_name}/rtmp-auth.service.epp"),
 
  }
  -> exec { '/usr/bin/systemctl daemon-reload for rtmp-auth':
    command => '/usr/bin/systemctl daemon-reload'
  }
  -> service {'rtmp-auth':
    ensure => 'running',
  }


}
