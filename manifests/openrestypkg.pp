# openresty pkg
class smsserver::openrestypkg {


  $pkgs = [
    'wget',
    'gnupg2',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


  exec { 'import resty gpg key (is not on keyserver)':
    command => 'wget -O- https://openresty.org/package/pubkey.gpg | apt-key add -',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
  }

  case $::lsbdistcodename {
 
    'trixie': {
    apt::source { 'resty_apt':
      ensure   => present,
      comment  => 'Debian stretch',
      location => 'http://openresty.org/package/debian',
      release  =>  'bookworm',
      repos    => 'openresty',
      include  => {
        'src' => false,
        'deb' => true,
      },
  }


    }


    default: {
   apt::source { 'resty_apt':
    ensure   => present,
    comment  => 'Debian stretch',
    location => 'http://openresty.org/package/debian',
    release  => $::lsbdistcodename ,
    repos    => 'openresty',
    include  => {
      'src' => false,
      'deb' => true,
    },
  }

 

    }
  }


  -> exec { 'apt-update for resty':
    command => '/usr/bin/apt-get update',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }

 if ! $::lsbdistcodename == 'trixie' { 
 
  exec { 'apt install for resty':
    command   => '/usr/bin/apt install -y --allow-downgrades openresty=1.19.9.1-1~bullseye1 openresty-opm=1.19.9.1-1~bullseye1 openresty-resty=1.19.9.1-1~bullseye1',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }

 } else {

  exec { 'apt install for resty':
    command   => '/usr/bin/apt install -y openresty openresty-opm openresty-resty',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

   }
  }
  -> exec { 'opm install for resty tls ':
    command   => 'opm install fffonion/lua-resty-acme',
    environment => [ 'HOME=/root'],
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }
  -> exec { 'create LE account key for resty':
    command   => 'openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:4096 -out /etc/openresty/account.key',
    logoutput => 'on_failure',
    creates => '/etc/openresty/account.key',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
  }

  -> exec { 'create LE fallback key for resty':
    command   => 'openssl req -newkey rsa:2048 -nodes -keyout /etc/openresty/default.key -x509 -subj "/C=NL/O=SSC/OU=Self Signed Cert/CN=*.com" -days 1365 -out /etc/openresty/default.pem',
    logoutput => 'on_failure',
    creates => '/etc/openresty/default.pem',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
  }

  
}
