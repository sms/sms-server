# ovenmedia
class smsserver::srtrelay (
)
{
  require smsserver::srt
  require smsserver::wsmsg

  $pkgs = [
    'golang',
    'build-essential',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})

 
  put::buildfromgit { 'srtrelay':
    revision => 'master',
    repo      => 'https://github.com/voc/srtrelay.git',
    commands  => [
      'go build -o srtrelay', 
      'cp srtrelay /usr/local/bin ; /bin/true'
    ]
  } 
  file { '/etc/srtrelay':
    ensure =>  directory
  }
  
  file { "/etc/srtrelay/config.toml":
    content => epp("${module_name}/srtrelay.config.toml.epp"),
    notify => Service['srtrelay'],
  }

  -> file { '/etc/systemd/system/srtrelay.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/srtrelay.service",
  }
  -> exec { '/usr/bin/systemctl daemon-reload for srtrelay':
    command => '/usr/bin/systemctl daemon-reload'
  }
  -> service {'srtrelay':
    ensure => 'running',
  }




}
