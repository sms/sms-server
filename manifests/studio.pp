class smsserver::studio (
  )
{

    require smsserver::nginxrtmp

    file { "/etc/nginx/modules-enabled/rtmp-studio.conf":
        content => epp("${module_name}/nginx-rtmp-studio.conf.epp"),
        notify => Service['nginx'],
      }



}
