# ovenmedia
class smsserver::ovenmedia (
)
{



    include docker

    docker::run { 'ovenmediaengine':
      image   => 'airensoft/ovenmediaengine:latest',
      env     => [  
                  "OME_RTMP_PROV_PORT=9935",
                  "OME_HLS_PUB_PORT=9876",
                  "OME_DASH_PUB_PORT=9876",

                 ],
      volumes => [ 
        'ome-origin-conf:/opt/ovenmediaengine/bin/origin_conf',
        'ome-edge-conf:/opt/ovenmediaengine/bin/edge_conf',
      ],
      ports   => [
        '9935:9935',
        '3333:3333',
        '3478:3478',
        '9876:9876',
        '9000:9000',
        '9999:9999/udp',
        '4000-4005:4000-4005/udp',
        '10006-10010:10006-10010/udp',
     ],
    }

}
