FROM debian:bullseye
ARG servertype
RUN apt update
RUN apt install -y puppet screen vim git

WORKDIR /etc/puppet/code/modules
RUN mkdir sms


#WORKDIR /etc/puppet/code/modules/sms

#RUN git clone https://git.puscii.nl/puppetexp/put.git
RUN git clone https://github.com/puppetlabs/puppetlabs-apt apt
RUN git clone https://github.com/puppetlabs/puppetlabs-stdlib stdlib
RUN git clone https://github.com/puppetlabs/puppetlabs-firewall firewall
RUN git clone https://github.com/puppetlabs/puppetlabs-git git
RUn git clone https://github.com/puppetlabs/puppetlabs-vcsrepo vcsrepo

RUN apt -y install libnginx-mod-rtmp nginx ffmpeg

RUN apt-get install -y supervisor
ADD . /etc/puppet/code/modules/smsserver
#RUN puppet module list --tree
#RUN puppet lookup --compile smsserver::record_path
#RUN puppet apply --modulepath=/etc/puppet/code/modules/ -e "lookup smsserver::record_path"

RUN mkdir -p /mnt/radio
RUN puppet apply --modulepath=/etc/puppet/code/modules/ -e "include smsserver::all"

#RUN apt-get -y install nginx libnginx-mod-rtmp
#RUN mkdir -p /opt/data/hls
ADD test/supervisord.conf /etc/supervisord.conf
CMD ["/usr/bin/supervisord"]
