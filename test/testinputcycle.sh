#!/usr/bin/env bash


count=0
while [ true ]
do
  for i in cycle1 cycle2 cycle3
  do
    count=$((count + 1))
    TEXT="TRY: $count"
    ffmpeg -re -f lavfi  -i "sine=frequency=1000" -f lavfi -i testsrc=size=1280x720:rate=30 \
     -vf "drawtext=text='$TEXT':font='Times New Roman':x=(main_w-text_w-10):y=(main_h-text_h-10):fontsize=332:fontcolor=black:box=1:boxcolor=white@0.5:boxborderw=5" \
     -vf drawtext="fontfile=monofonto.ttf: fontsize=96: box=1: boxcolor=black@0.75: boxborderw=5: fontcolor=white: x=(w-text_w)/2: y=((h-text_h)/2)+((h-text_h)/4): text='%{gmtime\:%H\\\\\:%M\\\\\:%S}'" \
     -vcodec libx264  -t 60 -f flv $SMS_INPUT_APP_URL/$i

    sleep 1
  done
done

#ffmpeg -re -f lavfi  -i "sine=frequency=1000" -f lavfi -i testsrc=size=1280x720:rate=30 \
#     -vf "drawtext=text='$TEXT':font='Times New Roman':x=(main_w-text_w-10):y=(main_h-text_h-10):fontsize=332:fontcolor=black:box=1:boxcolor=white@0.5:boxborderw=5" \
#     -vcodec libx264  -t 18 -f flv rtmp://$SMS_INPUT_HOST/input/$i
 
##-vf drawtext="fontfile=monofonto.ttf: fontsize=96: box=1: boxcolor=black@0.75: boxborderw=5: fontcolor=white: x=(w-text_w)/2: y=((h-text_h)/2)+((h-text_h)/4): text='%{gmtime\:%H\\\\\:%M\\\\\:%S}'"
