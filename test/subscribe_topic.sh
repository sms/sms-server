#!/usr/bin/env bash


curl -i \
    --header 'Accept: text/event-stream' \
    --header "Content-Type: application/json" \
    --get --data-urlencode 'payload={"topics":["foobar"]}' \
    https://noise.puscii.nl/sub
