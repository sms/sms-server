#!/usr/bin/env bash

# generates streams with random names to test the webpage


while [ true ]
do
  RAND=`echo $RANDOM | md5sum | head -c 10`
  ffmpeg -re -f lavfi  -i "sine=frequency=1000" -f lavfi -i testsrc=size=1280x720:rate=30 -vcodec libx264  -t 15 -f flv $SMS_INPUT_APP_URL/tsts$RANDOM

  sleep 5

done
