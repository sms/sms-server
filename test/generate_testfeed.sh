#!/usr/bin/env bash

. env.sh

ffmpeg -re -f lavfi  -i "sine=frequency=1000" -f lavfi -i testsrc=size=1280x720:rate=30 -vcodec libx264 -b:v 100K  -f flv $SMS_TRANSCODER_APP_URL/$SMS_LIVENAME

